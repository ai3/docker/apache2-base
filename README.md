
Docker base image with Apache2.

Comes with a minimal configuration for Apache2, including some common
sensible settings. Images wishing to use this one as a base should run
the relevant a2enmod / a2enconf commands to enable further modules or
configuration snippets.

By default the server will listen on port 8080, but it is possible to
override this at runtime by setting the `APACHE_PORT` variable in the
container environment.
