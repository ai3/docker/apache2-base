FROM registry.git.autistici.org/ai3/docker/s6-base:master

COPY conf /tmp/conf
COPY build.sh /tmp/build.sh

RUN /tmp/build.sh && rm /tmp/build.sh

