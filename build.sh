#!/bin/sh
#
# Install script for apache2-users inside a Docker container.
#

# Packages that are only used to build the container. These will be
# removed once we're done.
BUILD_PACKAGES="rsync"

# Packages required to serve the website and run the services.
# We have to keep the python3 packages around in order to run
# chaperone (installed via pip).
PACKAGES="
	curl
	apache2
	apache-exporter
	libapache2-mod-removeip
"

# Apache modules to enable.
APACHE_MODULES_ENABLE="
  headers
  proxy_http
  proxy_fcgi
  removeip
  setenvif
"

# Config snippets to enable.
APACHE_CONFIG_ENABLE="
  logging
"

# Config snippets to disable.
APACHE_CONFIG_DISABLE="
  other-vhosts-access-log
  serve-cgi-bin
"

# The default bitnami/minideb image defines an 'install_packages'
# command which is just a convenient helper. Define our own in
# case we are using some other Debian image.
if [ "x$(which install_packages)" = "x" ]; then
    install_packages() {
        env DEBIAN_FRONTEND=noninteractive apt-get install -qy -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" --no-install-recommends "$@"
    }
fi

set -x
set -e
umask 022

apt-get -q update
install_packages ${BUILD_PACKAGES} ${PACKAGES}

# Rsync our configuration, on top of /etc.
chmod -R go-w /tmp/conf
rsync -a /tmp/conf/ /etc/

# Set the Apache syslog program
APACHE_SYSLOG_TAG_DEFAULT=apache
echo "export APACHE_SYSLOG_TAG=\${APACHE_SYSLOG_TAG:-${APACHE_SYSLOG_TAG_DEFAULT}}" >> /etc/apache2/envvars
echo "export APACHE_LOG_LEVEL=\${APACHE_LOG_LEVEL:-warn}" >> /etc/apache2/envvars

# Fix Apache error logging.
sed -i -e 's@^ErrorLog.*$@ErrorLog "|/usr/bin/logger -p daemon.error -t \${APACHE_SYSLOG_TAG}"@' \
    -e 's,^LogLevel .*,LogLevel ${APACHE_LOG_LEVEL},' \
    /etc/apache2/apache2.conf

# Make APACHE_RUN_USER externally configurable (defaults to www-data if unset)
# and fix some paths used by apache2 to be container-friendly.
sed -i -e 's/^\(export APACHE_RUN_USER=\).*$/\1${APACHE_RUN_USER:-www-data}/' \
    -e 's,^\(export APACHE_PID_FILE=\).*$,\1/run/apache2/apache2.pid,' \
    -e 's,^\(export APACHE_RUN_DIR=\).*$,\1/run/apache2,' \
    -e 's,^\(export APACHE_LOCK_DIR=\).*$,\1/run/apache2,' \
    -e 's,^\(export APACHE_LOG_DIR=\).*$,\1/run/apache2,' \
    /etc/apache2/envvars

# Set the port that Apache will listen on.
APACHE_PORT_DEFAULT=8080
echo "export APACHE_PORT=\${APACHE_PORT:-${APACHE_PORT_DEFAULT}}" >> /etc/apache2/envvars
echo "export APACHE_EXPORTER_PORT=\`expr \$APACHE_PORT + 100\`" >> /etc/apache2/envvars

# Enable the default modules.
a2enmod -q ${APACHE_MODULES_ENABLE}
a2dismod -q -f ${APACHE_MODULES_DISABLE}
a2enconf -q -f ${APACHE_CONFIG_ENABLE}
a2disconf -q -f ${APACHE_CONFIG_DISABLE}

# Create the directories that Apache will need at runtime,
# since we won't be using the init script. To allow for apache
# not being started as root, create the directories with mode 1777.
install -d -m 1777 /run/apache2

# Remove packages used for installation.
apt-get remove -y --purge ${BUILD_PACKAGES}
apt-get autoremove -y
apt-get clean
rm -fr /var/lib/apt/lists/*
rm -fr /tmp/conf
